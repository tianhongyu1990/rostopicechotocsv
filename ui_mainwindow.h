/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpenFile;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QListWidget *listWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *buttonOpenFile;
    QPushButton *buttonAddLineFromLeft;
    QPushButton *buttonRemoveLineFromRIght;
    QPushButton *buttonGenerateOutPutFile;
    QListWidget *listWidgetOut;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuOpenFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(529, 453);
        actionOpenFile = new QAction(MainWindow);
        actionOpenFile->setObjectName(QStringLiteral("actionOpenFile"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        horizontalLayout->addWidget(listWidget);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        buttonOpenFile = new QPushButton(centralWidget);
        buttonOpenFile->setObjectName(QStringLiteral("buttonOpenFile"));

        verticalLayout->addWidget(buttonOpenFile);

        buttonAddLineFromLeft = new QPushButton(centralWidget);
        buttonAddLineFromLeft->setObjectName(QStringLiteral("buttonAddLineFromLeft"));

        verticalLayout->addWidget(buttonAddLineFromLeft);

        buttonRemoveLineFromRIght = new QPushButton(centralWidget);
        buttonRemoveLineFromRIght->setObjectName(QStringLiteral("buttonRemoveLineFromRIght"));

        verticalLayout->addWidget(buttonRemoveLineFromRIght);

        buttonGenerateOutPutFile = new QPushButton(centralWidget);
        buttonGenerateOutPutFile->setObjectName(QStringLiteral("buttonGenerateOutPutFile"));

        verticalLayout->addWidget(buttonGenerateOutPutFile);


        horizontalLayout->addLayout(verticalLayout);

        listWidgetOut = new QListWidget(centralWidget);
        listWidgetOut->setObjectName(QStringLiteral("listWidgetOut"));

        horizontalLayout->addWidget(listWidgetOut);


        verticalLayout_2->addLayout(horizontalLayout);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(24);

        verticalLayout_2->addWidget(progressBar);


        horizontalLayout_2->addLayout(verticalLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 529, 21));
        menuOpenFile = new QMenu(menuBar);
        menuOpenFile->setObjectName(QStringLiteral("menuOpenFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuOpenFile->menuAction());
        menuOpenFile->addAction(actionOpenFile);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOpenFile->setText(QApplication::translate("MainWindow", "OpenFile", 0));
        buttonOpenFile->setText(QApplication::translate("MainWindow", "Open\n"
"Input\n"
"File", 0));
        buttonAddLineFromLeft->setText(QApplication::translate("MainWindow", "Add\n"
"From\n"
"Left", 0));
        buttonRemoveLineFromRIght->setText(QApplication::translate("MainWindow", "Remove\n"
"From\n"
"Right", 0));
        buttonGenerateOutPutFile->setText(QApplication::translate("MainWindow", "Run", 0));
        menuOpenFile->setTitle(QApplication::translate("MainWindow", "OpenFile", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
