#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QFileDialog>
#include <QFile>
#include <QString>
#include <QTextStream>

#include <QListWidget>
#include <QWidgetItem>
#include <QDebug>
#include <QList>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openFile(const QString& file);
    void openFileDialog(bool selected);
    void performAddFromLeft(bool dummy);
    void performRemoveFromRight(bool dummy);
    void performRun(bool dummy);
    void doubleClickedInWidgetHook(QModelIndex);
    void doubleClickedOutWidgetHook(QModelIndex);

private:
    Ui::MainWindow *ui;
    QFileDialog dialog;
    QString openedFIleName;
    QList<int> indexList;
};

#endif // MAINWINDOW_H
