#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    dialog(this),
    openedFIleName(""),
    indexList()
{
    ui->setupUi(this);
    ui->listWidget->addItems(QStringList()<< "double click" << "OR" << "use button \"Add From Left\"" << "To add corresponding entry" << "into .CSV File");
    ui->listWidgetOut->addItems(QStringList()<< "CSV file will contain" << "colums in this order" << "" << "each column is the number after \':\'" << "collected from all frames");

    connect(ui->actionOpenFile,SIGNAL(triggered(bool)),this,SLOT(openFileDialog(bool)),Qt::UniqueConnection);
    connect(&(this->dialog),SIGNAL(fileSelected(const QString&)),this,SLOT(openFile(const QString&)),Qt::UniqueConnection);
    ui->menuBar->hide();
    this->setWindowTitle("ROSTopic Echo To CSV");
    //connect the push buttons
    //-----------------------
    connect(ui->buttonOpenFile,SIGNAL(clicked(bool)),this,SLOT(openFileDialog(bool)),Qt::UniqueConnection);
    connect(ui->buttonAddLineFromLeft,SIGNAL(clicked(bool)),this,SLOT(performAddFromLeft(bool)),Qt::UniqueConnection);
    connect(ui->buttonRemoveLineFromRIght,SIGNAL(clicked(bool)),this,SLOT(performRemoveFromRight(bool)),Qt::UniqueConnection);
    connect(ui->buttonGenerateOutPutFile,SIGNAL(clicked(bool)),this,SLOT(performRun(bool)),Qt::UniqueConnection);

    //connnect the double click event to add/remove
    //--------------------------
    connect(ui->listWidget, SIGNAL(doubleClicked(QModelIndex)),this,SLOT(doubleClickedInWidgetHook(QModelIndex)));
    connect(ui->listWidgetOut, SIGNAL(doubleClicked(QModelIndex)),this,SLOT(doubleClickedOutWidgetHook(QModelIndex)));


    ui->progressBar->setHidden(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile(const QString& file)
{
    ui->listWidget->addItem(file);
    QFile inFile(file);
    //check the integrity of the file
    //---------------------
    if(!inFile.exists())
    {
        return;
    }
    if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
              return;


    ui->listWidget->clear();
    ui->listWidgetOut->clear();
    indexList.clear();
    ui->progressBar->setValue(0);
    //open the file and read the first frame
    //-----------------------
    QTextStream in(&inFile);
    QString line("");
    while (!in.atEnd() && !line.contains("---"))
    {
      line = in.readLine();
      if(!line.contains("---"))
      {
          ui->listWidget->addItem(line);
      }
    }

    openedFIleName = file;
    inFile.close();
}

void MainWindow::openFileDialog(bool selected)
{
    Q_UNUSED(selected);
    dialog.show();
}
void MainWindow::performAddFromLeft(bool dummy)
{
    Q_UNUSED(dummy);
    if(ui->listWidget->selectedItems().isEmpty())
    {
        return;
    }
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    while(!items.isEmpty())
    {
        //##TODO##
        //we need to prevent an item from adding twice
        ui->listWidgetOut->addItem(items.first()->text());
        indexList.append(ui->listWidget->row(items.first()));
        items.pop_front();
    }
 }

void MainWindow::performRemoveFromRight(bool dummy)
{
    Q_UNUSED(dummy);
    if(ui->listWidgetOut->selectedItems().isEmpty())
    {
        return;
    }
    QList<QListWidgetItem *> items = ui->listWidgetOut->selectedItems();
    while(!items.isEmpty())
    {
        int row = ui->listWidgetOut->row(items.first());
        indexList.removeAt(row);
        delete items.first();
        items.pop_front();
    }
}

void MainWindow::performRun(bool dummy)
{
    Q_UNUSED(dummy);
    //check the integrity of the file
    //---------------------
    if(openedFIleName.isEmpty())
    {
        qDebug() << "openedFIleName empty! not going to run!";
       return;
    }
    QFile inFile(openedFIleName);
    if(!inFile.exists())
    {
        return;
    }

    //open files, do the preparation
    //-----------------
    ui->progressBar->setHidden(false);
    QByteArray line("");
    int lineCount = 0;
    int itrCount =0;
    int loopNum = ui->listWidget->count();
    int cummulativeSize = 0;
    int totalSize = inFile.size();

    if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }

    QString outptuFileName = openedFIleName.split(".").first() + ".csv";
    QFile outFile( outptuFileName );
    if ( ! outFile.open(QIODevice::ReadWrite) )
    {
        qDebug() << "cannot create output file with name: \n" << outptuFileName;
        return;
    }
    QTextStream outstream( &outFile );

    //build the lookup table
    //------------------------
    QVector<QList<int>> outputTable(loopNum);

    for(int i = 0; i < indexList.size() ; i ++)
    {
        outputTable[indexList.at(i)].append(i);
    }
    QVector<QByteArray> outLine(indexList.size());


    //write headers (1st row in csv file), they are names
    //-----------------------
    for(int row = 0; row < ui->listWidgetOut->count(); row++)
    {
             QListWidgetItem *item = ui->listWidgetOut->item(row);
             outstream << item->text().split(':').first() << ",";
    }
    outstream << "\n";

    //do the work with the input file
    //-----------------------
    while( !inFile.atEnd() )
    {
        //readline and maintain the size
        line = inFile.readLine();
        cummulativeSize += line.size();
         lineCount ++;

        //maintain the line counter and loop counter
        //---------------------
        if(loopNum == itrCount)
        {
            if(line.contains("---"))
            {
                for(int k = 0 ; k < outLine.size() ; k ++)
                {
                    outstream << outLine[k] << ",";
                }
                outstream << "\n";
                ui->progressBar->setValue(cummulativeSize*100/totalSize);

                itrCount =0;
                continue;
            }
            else
            {
                while(!line.contains("---") || !inFile.atEnd())
                {
                    line = inFile.readLine();
                    cummulativeSize += line.size();
                    qDebug() << "skipping line: " << lineCount
                             << "\nline: " << line;
                }
            }
        }
        else
        {
            if(!outputTable[itrCount].isEmpty())
            {
                QByteArray number = line.split(':').last();
                number = number.trimmed();
                for(int j =0; j < outputTable[itrCount].size() ; j ++)
                {
                    outLine[(outputTable[itrCount][j])] = number;
                }
            }
            itrCount ++;
        }

    }
    outstream.flush();
    outFile.close();
    inFile.close();
    ui->progressBar->setHidden(true);

    qDebug() << "cummulativeSize" << cummulativeSize << "\ntotalSize" <<totalSize;
}


void MainWindow::doubleClickedInWidgetHook(QModelIndex index)
{
    Q_UNUSED(index);
    performAddFromLeft(true);
}

void MainWindow::doubleClickedOutWidgetHook(QModelIndex index)
{
    Q_UNUSED(index);
    performRemoveFromRight(true);
}
